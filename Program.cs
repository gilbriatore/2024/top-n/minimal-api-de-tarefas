using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Configuração do banco de dados na API
// builder.Services.AddDbContext<AppDbContext>(
//     options => options.UseInMemoryDatabase("tarefas"));
// builder.Services.AddDatabaseDeveloperPageExceptionFilter();

builder.Services.AddDbContext<AppDbContext>();

//Configuração Swagger
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();
app.UseSwagger();
app.UseSwaggerUI();

app.MapGet("/", () => "API de Tarefas");

app.MapGet("/tarefas", async (AppDbContext db) =>
{
    //select * from Tarefas
    return await db.Tarefas.ToListAsync();


    // List<Tarefa> tarefas = new();
    // tarefas.Add(new Tarefa("Tarefa 1", false));
    // tarefas.Add(new Tarefa("Tarefa 2", true));
    // tarefas.Add(new Tarefa("Tarefa 3", false));
    // return tarefas;
});

app.MapGet("/tarefas/concluidas", async (AppDbContext db) =>
{
    //select * from Tarefas t where t.Concluida = true
    return await db.Tarefas.Where(t => t.Concluida).ToListAsync();

});
app.MapGet("/tarefas/{id}", async (int id, AppDbContext db) =>
{
    return await db.Tarefas.FindAsync(id)
        is Tarefa tarefa ? Results.Ok(tarefa) : Results.NotFound();
});

app.MapPost("/tarefas", async (Tarefa tarefa, AppDbContext db) =>
   {
       db.Tarefas.Add(tarefa);
       //insert into tarefas .....
       await db.SaveChangesAsync();

       return Results.Created($"/tarefas/{tarefa.Id}", tarefa);

   });


app.MapPut("/tarefas/{id}", async (
    int id, Tarefa tarefaAlterada, AppDbContext db) =>
{
    //select * from Tarefas where id = ?
    var tarefa = await db.FindAsync<Tarefa>(id);
    if (tarefa is null) return Results.NotFound();

    tarefa.Nome = tarefaAlterada.Nome;
    tarefa.Concluida = tarefaAlterada.Concluida;

    //update tarefas ...
    await db.SaveChangesAsync();

    return Results.NoContent();
});

app.MapDelete("/tarefas/{id}", async (int id, AppDbContext db) =>
{
    if (await db.Tarefas.FindAsync(id) is Tarefa tarefa)
    {
        db.Tarefas.Remove(tarefa);
        // delete from tarefas...
        await db.SaveChangesAsync();
        return Results.NoContent();
    }
    return Results.NotFound();
});

app.Run();




// var funcExcluir = () =>
//     {
//         return "EXCLUIR";
//     };

// string FuncExcluir()
// {
//     return "EXCLUIR";
// }


