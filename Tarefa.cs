

public class Tarefa
{

    public int Id { get; set; }
    public string? Nome { get; set; }
    public bool Concluida { get; set; }

    public Tarefa(string Nome, bool Concluida)
    {
        this.Nome = Nome;
        this.Concluida = Concluida;
    }

}