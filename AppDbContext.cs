

using Microsoft.EntityFrameworkCore;

class AppDbContext : DbContext
{

    public AppDbContext(
        DbContextOptions<AppDbContext> options)
        : base(options) { }

    // Configuração do banco de dados MySQL
    protected override void OnConfiguring(DbContextOptionsBuilder builder)
    {
        builder.UseMySQL("server=localhost;port=3306;database=tarefas;user=root;password=positivo");
    }


    // Classes que geram tabela no banco de dados...
    public DbSet<Tarefa> Tarefas => Set<Tarefa>();
    //public DbSet<Livro> Livros => Set<Livro>();
    //public DbSet<Pessoa> Pessoas => Set<Pessoa>();

}